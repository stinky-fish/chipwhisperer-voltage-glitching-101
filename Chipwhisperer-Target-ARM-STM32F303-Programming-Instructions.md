# Compile for ARM-STM32F303 on Mac OS

## Download and install Arduino IDE for your system:
https://www.arduino.cc/en/software 

## Add the appropriate Boards manager URL:  
Navigate to Arduino --> Preferences  
Add this URL (separated by a comma if there are more than one):  
https://github.com/stm32duino/BoardManagerFiles/raw/master/STM32/package_stm_index.json  
Close and re-open Arduino  

## Add the board support package:  
Navigate to Tools --> Boards --> Board Manager  
Search for 'stm32' and install the STM32 Cores package (this will take a couple minutes)   
Close and re-open Arduino  

## Choose the appropriate processor:  
Navigate to Tools --> Board  
Select the 'Generic STM32F3 series'  

## Setup an explicit build directory   
We need to tell Arduino where to put the build files  
Manually create a build directory in a convenient place using a terminal window  
```mkdir ~/Documents/Projects/STM32/Arduino```  
Open the prefrences.txt file which can be found by navigating to Arduino --> Preferences near the bottom of the menu    
Add a line to the end of the file that contains the path to your newly created directory:  
```build.path=/Users/jedeaton/Documents/Projects/STM32/Arduino```  
Close Arduino IDE, save the preferences.txt, re-open Arduino IDE  
Anytime you compile from now on you will find your-project-name.hex file in this newly created build directory that can be loaded onto the target board using the Chip Whisperer API or a debugger module.  

## Setup Blinky  
In Arduino IDE Navigate to File --> Examples --> 01.Basics --> Blink  
We'll need to modify the sketch to use the two LED's on the Chipwhisperer Arm Target board, they are connected to ports PC13 and P14  
The LED's are also terminated to the voltage rail through a current limiting resistor; we must need to drive the port LOW to illuminate the LED  
The LED signals in this application can be described as 'Active Low'  

Modify the Blink Sketch to look like this:  

```
const byte LedPin = PC13;
const byte LedPin2 = PC14;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  
  pinMode(LedPin, OUTPUT);
  pinMode(LedPin2, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LedPin, HIGH);       // turn the LED off (HIGH is the voltage level)
  digitalWrite(LedPin2, LOW);       // turn the LED on (LOW is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(LedPin, LOW);        // turn the LED off by making the voltage High
  digitalWrite(LedPin2, HIGH);      // turn the LED on by making the voltage Low
  delay(100);                       // wait for a second
}
```
Click 'Verify' (the Check Mark button) to compile your code.


## Program Target Board  
In a terminal window startup a python3 environment:   
```python3```  

Connect the Chipwhisperer to the USB port.  Use these python commands to initialize the chip whisperer and load the program.hex file onto the target: 

```
import chipwhisperer as cw
scope = cw.scope()
scope.default_setup()
scope.clock.clkgen_freq = 8E6
prog = cw.programmers.STM32FProgrammer
cw.program_target(scope, prog, "/Users/jedeaton/Documents/Projects/STM32/Arduino/program.hex", baud=38400)
```
If all goes well you'll see messages like this:
```
WARNING:root:Your firmware is outdated - latest is 0.23. Suggested to update firmware, as you may experience errors
See https://chipwhisperer.readthedocs.io/en/latest/api.html#firmware-update
Serial baud rate = 38400
Detected known STMF32: STM32F302xB(C)/303xB(C)
Extended erase (0x44), this can take ten seconds or more
Attempting to program 12159 bytes at 0x8000000
STM32F Programming flash...
STM32F Reading flash...
Verified flash OK, 12159 bytes
Serial baud rate = 38400
>>>
```

Once the board resets you should see the red and green LED's blinking alternately on the breakaway section of the Chipwhisperer target board.





