/*
 GLitch Loop 

 This code will trigger tio3 by driving it high
 Then it will compute a false conditional and execute blink_red if true
 Then it will trigger tio3 low
 Then it will delay 10 milliseconds
 Every once in a while it flashes the Green LED
 We should NEVER reach blink_red() during normal operation, if so we have successfully glitched past the false conditional statement

Chipwhisperer needs to watch for tio3 and glitch during the period when tio3 is high.
If Chipwhisperer stops getting a trigger then it should check for tio4 to be high to indicate successful glitch.
If Chipwhiusperer does not see tio4 high and has no trigger from tio3 then it should reset and start again b/c the program is no longer running.  

 This creates a trigger pulse of 1 milliseconds on tio3, during the pulse glitch code executes

*/

const byte LedRed = PC13;           //RED LED on chipwhisperer target board
const byte LedGreen = PC14;         //GREEN LED on target board
const byte tio3 = PA11;             //Trigger signal for ChipWhisperer
const byte tio4 = PA12;             //Use to indicate successful Glitch
volatile long int A = 0x1BAA;       //Some value to compare
int i = 0;                          //Loop variable
int j = 0;                          //Another Loop variable


void setup() {// the setup function runs after reset or power on
  pinMode(LedGreen, OUTPUT);     //Initialize pins as outputs
  pinMode(LedRed, OUTPUT);
  pinMode(tio3, OUTPUT);
  pinMode(tio4, OUTPUT);
  digitalWrite(tio3, LOW);       //Trigger signal is Active High
  digitalWrite(tio4, LOW);       //Sucessful Glitch signal is Active High
  digitalWrite(LedGreen, HIGH);  //LED's are Active Low
  digitalWrite(LedRed, HIGH);    
}


void blink_red(){                // blink red (forever) if we ever successfully glitch
  digitalWrite(tio4, HIGH);
  while (true){
    digitalWrite(LedRed, LOW);   // turn the red LED ON 
    delay(500);
    digitalWrite(LedRed, HIGH);  // turn the red LED OFF
    delay(500);
  }
}


void loop() {                 //main loop to trigger Chipwhisperer and attempt a false conditional each cycle
  i++;                        //increase loop counter variable
  digitalWrite(tio3, HIGH);   //trigger tio3
  delayMicroseconds(0.5);     //the glitch takes about 1 microsecond to start up 
  delayMicroseconds(0.5);     //these delays take about 0.25 microseconds each if you measure with an oscilloscope
  delayMicroseconds(0.5);     //and align the glitch sequence to happen as the next line(s) of code executes
  delayMicroseconds(0.5);
  ///////////////////////GLITCH CODE///////////////
  
  if (A == 0x2BAA){         
       blink_red();
  }
   
  //////////////////END GLITCH CODE/////////////////    
  digitalWrite(tio3, LOW);    //release tio3
  delay(10);                  //repeat test every 10 milliseconds, chipwhisperer misses triggers if you run it faster        
  
  if (i == 10) {                    //Flip the Green LED every once in a while so we know its working
    digitalWrite(LedGreen, HIGH);   // turn the green LED on (HIGH is the voltage level)
  }
  else if (i == 20) {
    digitalWrite(LedGreen, LOW);      
    i=0;
    }
}
