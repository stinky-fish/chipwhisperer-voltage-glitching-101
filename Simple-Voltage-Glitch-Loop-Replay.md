
# Simple Voltage Glitch Loop with Replay

Our glitch methodology uses two asynchronous processes (Chipwhisperer code and ARM target code).  Often times the chipwhishperer code will have moved ahead to the next glitch by the time it observes a success from the ARM target. This code seeks to determine exactly which glitch was the one that did the trick by replaying the last N number of glitches (usually it works well with 2 or 3).  

Glitch the power supply of the STM32F3X ARM target processor while its executing code using random glitch parameters until we measure a successful glitch.

Once we find a successful glitch replay the last N number of glitch profiles that were used to determine exactly which glitch profile it was that worked so well.  

## Prerequisite
Make sure you have [installed the Chipwhisperer software](/ChipWhisperer-Installation-Instructions.md) by doing the first two sections (prerequisite for Mac OS and Chipwhisperer install).  

** You do not need to install/run a jupyter environment unless you want/need to access/run the tutorial content.  We are just using a single python script and a single arduino sketch for our examples.  

Make sure you have [set up Arduino and have blinky working](/Chipwhisperer-Target-ARM-STM32F303-Programming-Instructions.md).  

## Arduino Code
Compile the [glitch_loop_1.ino sketch](/master/Arduino%20Sketches/glitch_loop_1.ino) in Arduino.  

The [Chipwhisperer Schematic](/Datasheets%20and%20Schematics/cw-lite-arm-main.pdf) shows how TIO3 and TIO4 wires connect the target STM32F3X processor to the Chipwhisperer FPGA.  The [Chipwhisperer API](https://chipwhisperer.readthedocs.io/en/latest/api.html) has definitions for easy configuration and use of TIO3 and TIO4.

This code will trigger the Chipwhisperer to start glitching by driving TIO3 net high.  Then it will compute a false conditional and execute blink_red if true.  Then it will drive TIO3 net low.  Then it will delay 10 milliseconds.  Every once in a while it flashes the Green LED so we can see it doing something.
 
** We should NEVER reach blink_red() during normal operation, if blink_red() happens then we have successfully glitched past the false conditional statement


## Python Code
Chipwhisperer needs to watch for TIO3 rising edge and glitch during the period when TIO3 is high.  If the Chipwhisperer stops getting a trigger then it should check for TIO4 to be high to indicate successful glitch.  If Chipwhisperer does not see TIO4 high and has no trigger from TIO3 then it should attempt to reset and start again b/c the program counter is corrupted and is no longer running. 

Once a glitch is found it will attempt to replay the last N number of glitches as directed by user input and in order to identify the exact glitch parameters that are more repeatable and reliable for use in an attack scenario.  

Run the [simple-voltage-glitch-loop-replay.py](/Python%20Scripts/simple-voltage-glitch-loop-replay.py) using python3 in a terminal window:

```  
python3 simple-voltage-glitch-loop-replay.py  

```  

When the program runs you should see output similar to this:  

```
jedeaton-> python3 simple-glitch-loop-replay.py
After a 'successful' glitch is found, how many parameters prior would you like to store? 10
Power Cycling Target...
Programming Target...
Detected known STMF32: STM32F302xB(C)/303xB(C)
Extended erase (0x44), this can take ten seconds or more
Attempting to program 14103 bytes at 0x8000000
STM32F Programming flash...
STM32F Reading flash...
Verified flash OK, 14103 bytes
WARNING:ChipWhisperer Scope:Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0f
WARNING:ChipWhisperer Scope:Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0e
... 
...
...
...
WARNING:ChipWhisperer Scope:Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0e
*******SUCCESSFUL GLITCH*********
Iteration = 153
Elapsed Time = 4.874392986297607
clk_src     = clkgen
width       = 1.5625
width_fine  = 0
offset      = 28.515625
offset_fine = 0
trigger_src = ext_single
arm_timing  = after_scope
ext_offset  = 1
repeat      = 5
output      = glitch_only

Glitch attempt rate = 31.388523746463996 glitches/sec
Entering Glitch Replay...
Power Cycling Target...
Programming Target...
Detected known STMF32: STM32F302xB(C)/303xB(C)
Extended erase (0x44), this can take ten seconds or more
Attempting to program 14103 bytes at 0x8000000
STM32F Programming flash...
STM32F Reading flash...
Verified flash OK, 14103 bytes
The last 10 glitch parameters used before success were:
3 38.671875 3 8.984375
5 38.28125 4 11.328125
4 41.015625 2 10.15625
2 10.9375 4 6.25
3 8.984375 1 10.546875
2 14.0625 4 3.90625
3 43.359375 3 13.671875
3 11.71875 2 6.640625
2 23.828125 1 12.5
5 28.515625 1 1.5625
Replaying last 10 parameters..
WARNING:ChipWhisperer Scope:Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0f
WARNING:ChipWhisperer Scope:Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0e
...
...
...
...
WARNING:ChipWhisperer Scope:Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0e
*******SUCCESSFULL REPLAY*********
Iteration = 760
Elapsed Time = 24.882009267807007
clk_src     = clkgen
width       = 1.5625
width_fine  = 0
offset      = 28.515625
offset_fine = 0
trigger_src = ext_single
arm_timing  = after_scope
ext_offset  = 1
repeat      = 5
output      = glitch_only

Glitch attempt rate = 30.54415709841037 glitches/sec
Terminating..```

