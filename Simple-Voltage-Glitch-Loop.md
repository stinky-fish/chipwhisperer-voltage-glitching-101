# Simple Voltage Glitch Loop 

Glitch the power supply of the STM32F3X ARM target processor while its executing code using random glitch parameters until we measure a successful glitch  

## Prerequisite
Make sure you have [installed the Chipwhisperer software](/ChipWhisperer-Installation-Instructions.md) by doing the prerequisite for Mac OS and Chipwhisperer install.  

** You do not need to install/run a jupyter environment unless you want/need to access/run the tutorial content.  We are just using a single python script and a single arduino sketch for our examples.  

Make sure you have [set up Arduino and have blinky working](/Chipwhisperer-Target-ARM-STM32F303-Programming-Instructions.md).  

## Arduino Code
Compile the [glitch_loop_1.ino sketch](/Arduino%20Sketches/glitch_loop_1.ino) in Arduino.  

The [Chipwhisperer Schematic](/Datasheets%20and%20Schematics/cw-lite-arm-main.pdf) shows how TIO3 and TIO4 wires connect the target STM32F3X processor to the Chipwhisperer FPGA.  The [Chipwhisperer API](https://chipwhisperer.readthedocs.io/en/latest/api.html) has definitions for configuration and use of TIO3 and TIO4.

This code will trigger the Chipwhisperer to start glitching by driving TIO3 net high.  Then it will compute a false conditional and execute blink_red if true.  Then it will drive TIO3 net low.  Then it will delay 10 milliseconds.  Every once in a while it flashes the Green LED so we can see it doing something.
 
** We should NEVER reach blink_red() during normal operation, if blink_red() happens then we have successfully glitched past the false conditional statement


## Python Code
Chipwhisperer needs to watch for TIO3 rising edge and glitch during the period when TIO3 is high.  If the Chipwhisperer stops getting a trigger then it should check for TIO4 to be high to indicate successful glitch.  If Chipwhisperer does not see TIO4 high and has no trigger from TIO3 then it should reset and start again b/c the program is no longer running. 

Run the [simple-voltage-glitch-loop.py](/Python%20Scripts/simple-voltage-glitch-loop.py) using python3 in a terminal window:

```  
python3 simple-voltage-glitch-loop.py  

```  

When the program runs you should see output similar to this:  

```
-> python3 simple_glitch_2.py
Serial baud rate = 38400
Power Cycling Target...
Programming Target...
Serial baud rate = 38400
Detected known STMF32: STM32F302xB(C)/303xB(C)
Extended erase (0x44), this can take ten seconds or more
Attempting to program 12351 bytes at 0x8000000
STM32F Programming flash...
STM32F Reading flash...
Verified flash OK, 12351 bytes

Iteration #1000
Iteration #2000
WARNING:root:Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0f

```
When a timeout happens it probably means that the target program and/or program counter is no longer running because of a glitch, it needs to be reset or maybe even reprogrammed to get going again.  

Values larger than ~10 for scope.glitch.width in the simple_glitch_2.py script may glitch the STM32F3X target processor hard enough to make the processor/program counter stop running.  Driving the reset pin LOW to reset it can get it going again. 

Values larger than ~12 for scope.glitch.width may glitch the target so hard that it does not even recover after a reset event.  

The python script will reset the target automatically when no trigger is observed.  If a trigger still does not happen (after 5 reset tries) then it will power cycle the target, reprogram the target, and restart the glitch loop. 

Eventually it should find a glitch and exit the program:

```
WARNING:root:Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0f
WARNING:root:Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0e
WARNING:root:Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0f
WARNING:root:Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0c
Iteration #4000
WARNING:root:Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0f
WARNING:root:Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0e
WARNING:root:Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0f
WARNING:root:Timeout in OpenADC capture(), no trigger seen! Trigger forced, data is invalid. Status: 0e
*******SUCCESSFULL GLITCH*********
Iteration = 4031
Elapsed Time = 138.62592911720276
clk_src     = clkgen
width       = 9.765625
width_fine  = 0
offset      = 14.453125
offset_fine = 0
trigger_src = ext_single
arm_timing  = after_scope
ext_offset  = 3
repeat      = 3
output      = glitch_only

Glitch attempt rate = 29.07825415973911 glitches/sec

```

No telling when you'll get one, but it usually happens every 100-200 seconds.  

If you want to see the action place an oscilloscope probe onto the TIO3 pin on the target board and press the auto-scale button.

You'll see all the glitches occuring on top of the trigger pulse similar to the pink waveform in this picture:

![delayed-target-code-hits-glitch-window](/Pictures/delayed-target-code-hits-glitch-window.png)


