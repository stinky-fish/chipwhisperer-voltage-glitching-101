# Glitching with Chipwhisperer

This content is intened for use with the [Chipwhisperer-Lite ARM solution](https://www.newae.com/products/NAE-CWLITE-ARM)  

The goal of this content is to create simple python script(s) to run on the chip whisperer along with simple Arduino program(s) to run on the (ARM) target that successfully demonstrate voltage glitching.    

Here you can find the [Chipwhisperer-Lite ARM schematic](/Datasheets%20and%20Schematics/cw-lite-arm-main.pdf) and [STM32F303 ARM Target Datasheet](/Datasheets%20and%20Schematics/STM32F303RC-datasheet-dm00058181-1797663.pdf)

Start out by [setting up your Chipwhisperer environment](/ChipWhisperer-Installation-Instructions.md) 

Next you need to [setup your Arduino Environment](Chipwhisperer-Target-ARM-STM32F303-Programming-Instructions.md) for programming the ARM target

You may need to [update the firmware of your Chipwhisperer](/Chipwhisperer-Firmware-Update.md) 

Here is a [simple voltage glitch tutorial](/Chipwhisperer-Simple-Voltage-Glitch.md) to explain our basic process for voltage glitching that can be observed/measured with an oscilloscope.  

Here is a [simple voltage glitch loop tutorial](/Simple-Voltage-Glitch-Loop.md) that performs glitches using randomized glitch parameters until a successful glitch is found.

Here is a [simple voltage glitch loop replay tutorial](/Simple-Voltage-Glitch-Loop-Replay.md) that performs glitches using randomized glitch parameters until a successful glitch is found and then replays the last N number of attempted glitches to determine exactly which glitch profile was most successful.  
  
