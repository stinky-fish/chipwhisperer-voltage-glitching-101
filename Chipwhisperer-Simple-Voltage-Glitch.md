# Simple Glitch Example

We want to glitch the target ARM processor when we are performing a false conditional statement (if 1 == 2 then) in order to get a false conditional result and execute code that should never happen under normal circumstances.  

## Trigger Pulse Code

Consider the following Arduino Code that makes the shortest possible trigger pulse for our ARMSTM32F3X target using digitalWrite() functions:  

```  
void loop() {                
  digitalWrite(tio3, HIGH);  //trigger tio3  
  digitalWrite(tio3, LOW);   //release tio3  
  delay(4);                  //do this pulse every 4 milliseconds
}  
```  
Looking at the TIO3 signal on the oscilloscope (red color) we see a pulse width of 944 nanoseconds (~ 1 millisecond) repeating every 4 milliseconds (a single pulse shown below).  The orange dotted lines are the cursor positions and the time/voltage measurements associated with the cursors are shown on the side:  

![Single Pulse Width](/Pictures/Single-Pulse-Width.png)


## Target Pulse Width
By adding in our target line of code to the loop:

```  
void loop() {                
  digitalWrite(tio3, HIGH);  //trigger tio3  
  ///////////////////Target Code///////////
  if (A == 0x2BAB){         
       blink_red();
  }
  ///////////////End Target Code///////////
  digitalWrite(tio3, LOW);   //release tio3  
  delay(4);                  //do this pulse every 4 milliseconds
}  
```  
We can measure with the oscilloscope that it adds about 224 nanoseconds (~ 0.25 milliseconds) to the pulse width:

![Target Pulse Width](/Pictures/single-pulse-plus-target-code.png)

## Target Code Window
Now we know it takes that it takes roughly 0.5 milliseconds to raise the trigger, 0.25 milliseconds to do our conditional statement, and another 0.5 milliseconds to lower the trigger.  We can use the cursors on the scope to show the approximate time window that we'll need to glitch in order to hit our conditional statement:

![Target Code Window](/Pictures/target-code-window.png)

## Glitch Pulses

This python3 [simple-glitch.py](/Python%20Scripts/simple-glitch.py) code will use the Chipwhisperer to glitch the target during each pulse.

We can see the actual glitch pulses by measuring the input to the T4 glitch transistor in the [Chipwhisperer-Lite Schematic](/ChipWhisperer/Datasheets/cw-lite-arm-main%20(1).pdf) (I soldered a wire onto the appropriate pin of T4, its a long wire so there is a lot of artificial ringing happening).

Looking on the oscilloscope we see that the T4 input glitch pulses happen AFTER the target code has executed (blue signal).  This is no good; we are alwaysing missing the window to glitch while the target code is executing.

![Glitch-missing-target-window](/Pictures/glitch-misses-target-window.png)

## Delay Target Code to align the glitches

We can modify the X1 cursor on previous Oscilloscope capture to mark the time window from target code starting until the falling edge of tio3 trigger pulse.  We know that glitches should be already happening when this window starts or else we will miss our target code:

![Target Window Distance from falling edge of trigger](/Pictures/target-window-distance-from-falling-trigger.png)

In order to align the glitches to the target code window we need to add a slight amount of delay in after we raise the trigger in order to give the glitch module time to fire.  We can do this by adding microsecond delays to the Arduino ARMSTM32F3X target code:

```
void loop() {                
  digitalWrite(tio3, HIGH);  //trigger tio3  
  delayMicroseconds(0.5);     //the glitch takes about 1 microsecond to start up 
  delayMicroseconds(0.5);     //arduino says the delays are not so accurate under 3 microseconds
  delayMicroseconds(0.5);     //these delays take about 0.25 microseconds each if you measure with an oscilloscope
  delayMicroseconds(0.5);     //and align the glitch sequence to happen as the next line(s) of code executes
  
  ///////////////////Target Code///////////
  if (A == 0x2BAB){         
       blink_red();
  }
  ///////////////End Target Code///////////
  digitalWrite(tio3, LOW);   //release tio3  
  delay(4);                  //do this pulse every 4 milliseconds
}  

```

After adding the delay statements after raising the trigger and before running the target code we can see the glitch pulses happening during the time when our target code is executing:
![](/Pictures/delayed-target-code-hits-glitch-window.png)

This is what we want to see!

Now try it out - use Arduino to compile the [glitch_loop1.ino](/Arduino%20Sketches/glitch_loop_1.ino) and use Python3 to run the [simple-glitch.py](/Python%20Scripts/simple-glitch.py) to create a similar picture on your scope.  

**It is important to understand these basic timing considerations to make sure we hit our target code with glitches.  It may be necessary to experiment with different delay periods on the Arduino side if we don't get good results with these settings.    







