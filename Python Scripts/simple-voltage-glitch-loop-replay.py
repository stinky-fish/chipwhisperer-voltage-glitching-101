import chipwhisperer as cw
import time
import sys
import random
import pdb 
import os
import collections 

N = int(input("After a 'successful' glitch is found, how many parameters prior would you like to store? "))
#loop = int(input("After a 'successful' replay is found, how many times would you like replay parameter? "))
replayQueue = collections.deque(maxlen=(N*4)) #buffer holds N previous glitch cycles, each cycle has 4 parameters

##############Variables for counting stuff########
errorCount = 0
successCount = 0
flag = 0
REPLAY_NUM = 0

################Create the Scope##################
scope = cw.scope()    #scope object from chipwhisperer api
scope.default_setup() #default values in case it is still running and glitching from previous configurations

###############Create the Target and programmer objects#############
target = cw.target(scope)	#target object from chipwhisperer api
prog = cw.programmers.STM32FProgrammer

##############Function to program the Target############
def program_target():
	print("Programming Target...")
	cw.program_target(scope, prog, "/Projects/STM32/Arduino/glitch_loop_1.ino.hex", baud=38400)

#####function to Reset the Target#####################
def reset_target():
	scope.io.nrst = False 		#nrst (not reset) is the active low reset net/pin on the target STM32F3X, drive low to reset
	time.sleep(0.010) 		#datasheet shows that ~ 3 milliseconds is min to reset, lets do 10 milliseconds
	scope.io.nrst = True 
	time.sleep(0.010)

########function to cycle power to the target################
def cycle_target_power():
	print("Power Cycling Target...")
	scope.io.target_pwr = False 	#turn off target_pwr, the active high net to control the power supply to the STM32F3X target
	time.sleep(0.25) 		#1/4 of a second seems to work fine to turn off power supply for a hard reset of the STM32FX target
	scope.io.target_pwr = True	#turn power supply back on to STM32F3X target
	time.sleep(0.25)		


def glitch_replay(scope):
	global N
	global loopCount
	global start
	CPRS()
	replayPrintCount = 0
	z = 0 
	print("The last " + str(N) + " glitch parameters used before success were:")
	start = time.time()
	while (z < (N*4)):
        	#print the repeat, offset, ext_offset, and width for each of our loop(s)
        	print(str(replayQueue[z]) + " " + str(replayQueue[z+1]) + " " + str(replayQueue[z+2]) + " " + str(replayQueue[z+3]))
        	z+=4  #increase index by 4 to grab the next set of glitch parameters
	print("Replaying last " + str(N) + " parameters..")
	z = 0
	while (True):
		loopCount += 1                                          #how many times we try to glitc
		replayPrintCount += 1                                         #print tracking variable for giving status to terminal every so often
        	#pdb.set_trace()
		scope.glitch.repeat = replayQueue[z]           #glitch pulses per attempt
		scope.glitch.offset = replayQueue[z+1]         #random offset value between 1 and 45 for how long from a rising clock edge to a glitch pulse rising edge, as a percentage of one clock period
		scope.glitch.ext_offset = replayQueue[z+2]     #random value value between 1 and 5 for how long the glitch module waits between a trigger and a glitch (in clock cycles)
		scope.glitch.width = replayQueue[z+3]          #random decimal value Width of glitch pulse between 1 and 14
		#f.close()
		z+=4
		if (z == (N*4)):
			z = 0 
		if (replayPrintCount == 1000):                                #printout loop status every 1000 cycles
			print("Replay Iteration #" + str(loopCount))                   #number of times we've tried to glitch
			replayPrintCount = 0                                          #reset print counter
		#pdb.set_trace()
		s = glitch_target(False)    
		if (s  == False):                               #if false then there was no trigger on TIO3 from the STM32F3XX target, processor is hung
			errorCount += 1                                         #keep a count of how many attempts to reset
			reset_target()                                          #reset the target by toggling the reset pin
		else:                                                   #if true we got a good glitch attempt
	    
			errorCount = 0                                          #reset the error counter
		if (errorCount == 5):                                   #We have had 5 passes at glitching without a trigger; resetting is not working to recover the target
			print("Unresponsive Target...")                         #status to terminal
			cycle_target_power()                                    #Turn off and on the power to the STM32F3XX processor
			scope = cw.scope()                                      #Make a new and clean chipwhisperer scope object to work with 
			scope.default_setup()                                   #Make sure we are not still not glitching from the last time
			program_target()                                        #Re-Program the target STM32F3X processor

			reset_target()                                          #Reset it to make sure its running
			setup_scope()                                           #Setup the Chipshisperer scope object for capture
			errorCount = 0;                                         #Reset the error counter for next the time it stalls`
		

###########function to perform a glitch##############
def glitch_target(glitchr):
	global start
	global loopCount
	#print(scope.glitch)
	scope.arm()  			#arm the scope to fire a glitch when it receives a trigger (connected to TIO3 pin/net on the target board)
	timeout = 30 			#target fires every 10 milliseconds, this is enough for 2-3 triggers in case we miss one
	REPLAY_NUM = 0
	#ret = scope.capture 
	while target.isDone() is False and timeout: 		#setup while loop to run until it gets a trigger or until the 30 millisecond adc timeout
		timeout -= 1  					#decrement timeout counter 
		time.sleep(1/1000) 				#sleep for 1 millisecond 	
	try:							#every other millisecond check for a result
		
		ret = scope.capture()				#get the return value from the chipwhisperer scope object
		if ret: 					#if true it means the chipwhisperer scope module did not see a trigger on TIO3
			#maybe put delay here 
			if (scope.advancedSettings.cwEXTRA.readTIOPin(4) == True):  	#if TIO4 pin is high then we have a glitch, the blink_red() routine is running!!

				#pdb.set_trace()
				elapsedTime = time.time() - start					#how much time since the program started (in seconds)
				glitchRate = loopCount/elapsedTime					#number of glitches attempted divided by number of seconds  
				if (glitchr):
					print("*******SUCCESSFUL GLITCH*********")
				else:
					print("*******SUCCESSFULL REPLAY*********")
													#write results to terminal output
				print("Iteration = " + str(loopCount))					#how many attempts it took
				print("Elapsed Time = " + str(elapsedTime))				#how long it took
				print(scope.glitch)							#Glitch parameters that were used
				print("Glitch attempt rate = " + str(glitchRate) + " glitches/sec")	#How fast we are attempting glitching
				if (glitchr == True):
					loopCount = 0 
					print("Entering Glitch Replay...")						#Exit program on success 
					glitch_replay(scope)
				else:
					exit("Terminating..")

			return False							#if TIO4 is not high and we have no trigger from TIO3 something is wrong, the glitch attempt has failed
	except IOError as e: 			#catch any chipwhisperer errors
		print('Iteration #' + str(loopCount))
		print('IOError: %s' % str(e)) 		#print chipwhisperer errors to the terminal 
	return True 				#The scope got a trigger from TIO3 and TIO4 is not high; the glitch attempt was successful, but had no result




############Setup the Chipwhisperer Scope
def setup_scope():
	scope.default_setup() 			#set all else to sane values
	scope.glitch.repeat = 6			#how many time it fires a glitch after a trigger
	scope.adc.timeout = 0.1 		#how long before the adc trigger times out, set 20 miliseconds to make sure we get at least one TIO3 trigger that happens every 10 milliseconds
	scope.glitch.ext_offset = 2		#how long to wait to start glitching after a trigger
	scope.gain.gain = 25         		#gain for scope capture, 25 results in (voltage) swing of about 0.6V (0.3V to negative 0.3)
	scope.adc.samples = 3000     		#how many samples to capture for scope picture (more samples = longer capture time and resulting graph picture)
	scope.adc.offset =  1250     		#vertical waveform position in the graph picture, equivalent to verti
	scope.adc.basic_mode = "rising_edge"  	#trigger the adc on the rising edge
	
	#scope.clock.clkgen_src = "extclk"	#First step to improve glitch precision
	#scope.clock.clkgen_mul = 10		#Second step to improve glitch precision
	#scope.clock.clkgen_div = 1
	scope.clock.clkgen_freq = 7370000	#how fast to run the chipwhisperer clock
	scope.clock.adc_src = "clkgen_x4"	#how fast to run the sampling clock for the adc
	scope.trigger.triggers = "tio3"  	#our arduino code drives tio3 high just before running and releases it is finished 
	scope.io.tio4 = "high_z" 		#our arduino code drives tio4 pin high if blink_red() is called, set as high impedance input
	scope.glitch.clk_src = "clkgen"		#use clock source for glitching 
	scope.glitch.output = "glitch_only" 	#insert one glitch cycle based on width, offset, repeat, etc. paramters
	scope.glitch.trigger_src = "ext_single" #glitch only after scope.arm() is called
	scope.io.glitch_lp = "False" 		#disable low power glitch transistor output
	scope.io.glitch_lp = "True" 		#enable LP glitch transistor output 
	scope.io.glitch_lp = "False" 		#disble LP glitch transistor output again for good measure
	scope.io.glitch_lp = "True"		#enable LP glitch transistor output



#############Cycle Power, Program, and Reset the target, get the chipwhisperer scope ready############
def CPRS():
	cycle_target_power()
	program_target()
	reset_target()
	setup_scope()

##############Arm the scope and set a timeout incase nothing happens#########
CPRS() 							#cycle power, program, reset target, setup scope
global loopCount
global start
loopCount = 0
start = time.time()
printCount = 0
while (True):
	loopCount += 1   					#how many times we try to glitch
	printCount += 1						#print tracking variable for giving status to terminal every so often
	scope.glitch.repeat = random.randrange(1,8,1)		#random number between 1 and 8 for glitch pulses per attempt
	scope.glitch.offset = random.uniform(1, 45)		#random offset value between 1 and 45 for how long from a rising clock edge to a glitch pulse rising edge, as a percentage of one clock period
	scope.glitch.ext_offset = random.randrange(1,5,1)	#random value value between 1 and 5 for how long the glitch module waits between a trigger and a glitch (in clock cycles)
	scope.glitch.width = random.uniform(1, 14) 		#random decimal value Width of glitch pulse between 1 and 14
	replayQueue.append(scope.glitch.repeat) 		#add the repeat valu to the replay queue
	replayQueue.append(scope.glitch.offset) 		#add the offset value
	replayQueue.append(scope.glitch.ext_offset) 		#add the ext_offset value
	replayQueue.append(scope.glitch.width) 			#add the width value
	#f.close()
	if (printCount == 1000):  				#printout loop status every 1000 cycles
		print("Iteration #" + str(loopCount))			#number of times we've tried to glitch
		printCount = 0						#reset print counter
	#pdb.set_trace()
	s = glitch_target(True)			
	if (s  == False): 					#if false then there was no trigger on TIO3 from the STM32F3XX target, processor is hung
		errorCount += 1 					#keep a count of how many attempts to reset
		reset_target()						#reset the target by toggling the reset pin
	else:							#if true we got a good glitch attempt
		
		errorCount = 0						#reset the error counter
	if (errorCount == 5):     				#We have had 5 passes at glitching without a trigger; resetting is not working to recover the target
		print("Unresponsive Target...")				#status to terminal
		cycle_target_power() 					#Turn off and on the power to the STM32F3XX processor
		scope = cw.scope() 					#Make a new and clean chipwhisperer scope object to work with 
		scope.default_setup() 					#Make sure we are not still not glitching from the last time
		program_target()   					#Re-Program the target STM32F3X processor
		reset_target()	  					#Reset it to make sure its running
		setup_scope()	 					#Setup the Chipshisperer scope object for capture
		errorCount = 0;	 					#Reset the error counter for next the time it stalls`

