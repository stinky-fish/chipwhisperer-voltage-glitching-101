import chipwhisperer as cw
import matplotlib.pylab as plt
import time
import matplotlib.pyplot as plt
from tqdm.notebook import trange
import struct
import chipwhisperer.common.results.glitch as glitch
import pdb

################Create the Scope###############
scope = cw.scope()
scope.default_setup()

################Create the glitch controller#######
gc = glitch.GlitchController(groups=["success", "reset", "normal"], parameters=["width", "offset"])
gc.set_range("width", 0, 48)
gc.set_range("offset", -48, 48)
gc.set_global_step([8, 4])
scope.glitch.repeat = 10
scope.adc.timeout = 0.1
scope.glitch.ext_offset = 2

###############Create the Target#############
target = cw.target(scope)

##############Program the Target############
prog = cw.programmers.STM32FProgrammer
cw.program_target(scope, prog, "/Projects/STM32/Arduino/glitch_loop_1.ino.hex", baud=38400)

#####Reset the Target#####################
print("Resetting Target...")
scope.io.nrst = False #or you can set to None if you want it to float
time.sleep(0.25)
scope.io.nrst = True

############Setup the Scope
scope.gain.gain = 25 #25 results in (voltage) swing of about 0.6V (0.3V to negative 0.3)
scope.adc.samples = 3000
scope.adc.offset =  1250 
scope.adc.basic_mode = "rising_edge"
scope.clock.clkgen_freq = 7370000
scope.clock.adc_src = "clkgen_x4"
scope.trigger.triggers = "tio3"  ##our arduino code drives tio3 high just before running and releases it is finished 
#scope.io.hs2 = "glitch" 

pdb.set_trace()
############Setup the Glitch Parameters
scope.glitch.clk_src = "clkgen" 
scope.glitch.output = "glitch_only" #insert glitch for clock cycle based on width and offset
scope.glitch.trigger_src = "ext_single" #glitch only after scope.arm() is called
scope.io.glitch_lp = "False" #enable LP glitch #reset the glitch module a couple times
scope.io.glitch_lp = "True" #enable LP glitch
scope.io.glitch_lp = "False" #enable LP glitch
scope.io.glitch_lp = "True" #enable LP glitch
scope.glitch.offset = -49
scope.glitch.width = 10
print(scope.glitch) #show glitch paramters

##############Arm the scope and set a timeout incase nothing happens#########

while (True):
		#scope.arm()
		timeout = 3 #our pulse width is about 3 microseconds long, our target code is all done by then
		ret = scope.capture
		while target.isDone() is False and timeout:
			timeout -= 1
			time.sleep(1/1000000) #in microseconds
		try:
			ret = scope.capture()
			if ret:
				print('Timeout happened during acquisition')
		except IOError as e:
			print('IOError: %s' % str(e))
