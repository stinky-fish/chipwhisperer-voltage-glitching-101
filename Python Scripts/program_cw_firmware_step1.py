
#Script to put the chipwhisperer in bootloader mode before programming firmware!!
#Unplug and replug the chipwhisperer from USB after you run this and before doing program_cw_step2.py
import chipwhisperer as cw
scope = cw.scope()
programmer = cw.SAMFWLoader(scope=scope)
programmer.enter_bootloader(really_enter=True)
print("Unplug Chipwhisperer from USB for 2 secs and replug!!")





