## To Update firmware on the Chipwhisperer:

Clone the latest repository from Chipwhisperer using git in a terminal window:  
```git clone https://github.com/newaetech/chipwhisperer.git```

The latest firmware file for our cw-lite board will be located inside the repo:  
```../chipwhisperer/hardware/capture/chipwhisperer-lite/sam3u_fw/SAM3U_VendorExample/Debug/SAM3U_CW1173.bin```

Use the [program_cw_firmware_step1.py](/Python%20Scripts/program_cw_firmware_step1.py) script to put the Chipwhisperer into bootloader mode using python3 in a terminal window:  
```python3 program_cw_firmware_step1.py```  

After running step_1 unplug your Chipwhisperer from USB for 2 or more seconds and then replug.  

Look for the Chipwhisperer to appear in the device list as 'tty.usbmodem142201' or similar using the terminal command:  
```ls /dev```

Modify the [program_cw_fimrware_step2.py](/Python%20Scripts/program_cw_firmware_step2.py) script to point to the correct device name as well as the correct location of the SAM3UCW1173.bin file that was recently cloned.  You will need to modify this line in the script:  
```programmer.program ("/dev/tty.usbmodem142201", "/chipwhisperer/hardware/capture/chipwhisperer-lite/sam3u_fw/SAM3U_VendorExample/Debug/SAM3U_CW1173.bin")```

Use python3 to run program_cw_firmware_step2.py in a terminal to upload the new firmware image to the device:  
```python3 program_cw_firmware_step2.py ```





